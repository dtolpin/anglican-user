(defproject dtolpin/anglican-user "1.2.1"
  :description "Template repository for starting an Anglican project"
  :url "http://bitbucket.org/dtolpin/anglican-user"
  :signing {:gpg-key "david.tolpin@gmail.com"}
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repositories [["releases" {:url "https://repo.clojars.org"
                              :creds :gpg}]]
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [nstools "0.2.4"]
                 [dtolpin/anglican "1.2.1"]]
  :plugins [[org.clojars.benfb/lein-gorilla "0.7.0"]]
  :resource-paths ["programs"]
  :main ^:skip-aot anglican.core)
